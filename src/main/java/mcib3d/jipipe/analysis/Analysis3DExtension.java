package mcib3d.jipipe.analysis;

import net.imagej.ImageJ;
import org.hkijena.jipipe.*;
import org.hkijena.jipipe.api.JIPipeAuthorMetadata;
import org.hkijena.jipipe.api.JIPipeProgressInfo;
import org.hkijena.jipipe.extensions.parameters.library.markup.HTMLText;
import org.hkijena.jipipe.extensions.parameters.library.primitives.list.StringList;
import org.hkijena.jipipe.utils.ResourceUtils;
import org.hkijena.jipipe.utils.UIUtils;
import org.scijava.Context;
import org.scijava.plugin.Plugin;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Plugin(type = JIPipeJavaExtension.class)
public class Analysis3DExtension extends JIPipeDefaultJavaExtension  {
    @Override
    public StringList getDependencyCitations() {
        return new StringList("3D Suite");
    }

    @Override
    public String getCitation() {
        return "";
    }

    @Override
    public String getName() {
        return "3D Analysis Ellipsoid";
    }

    @Override
    public HTMLText getDescription() {
        return new HTMLText("3D Analysis Ellipsoid");
    }

    @Override
    public List<JIPipeAuthorMetadata> getAuthors() {
        JIPipeAuthorMetadata authorMetadata = new JIPipeAuthorMetadata("Dr","Thomas","Boudier",new StringList("AMU"),"https://mcib3d.frama.io/3d-suite-imagej/","thomas boudier",false,true);
        List<JIPipeAuthorMetadata> list = new ArrayList<>();
        list.add(authorMetadata);

        return list;
    }

    @Override
    public String getWebsite() {
        return "https://mcib3d.frama.io/3d-suite-imagej/";
    }

    @Override
    public String getLicense() {
        return "GPL3";
    }

    @Override
    public URL getLogo() {
        return ResourceUtils.getPluginResource("logo-400.png");
    }

    @Override
    public void register(JIPipe jiPipe, Context context, JIPipeProgressInfo jiPipeProgressInfo) {
        registerNodeType("suite3d-analysis-ellipsoid", Analysis3DEllipsoidAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-compactness", Analysis3DCompactnessAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-distancecenter", Analysis3DDistanceCenterAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-feret", Analysis3DFeretAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-intensity", Analysis3DIntensityAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-intensity-histogram", Analysis3DIntensityHistogramAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-surface", Analysis3DSurfaceAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-volume", Analysis3DVolumeAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
    }

    @Override
    public String getDependencyId() {
        return "";
    }

    @Override
    public String getDependencyVersion() {
        return "";
    }

    public static void main(final String... args) {
        final ImageJ ij = new ImageJ();
        ij.ui().showUI(); // If your ImageJ freezes, you can leave this out. JIPipe will show anyways.
        ij.command().run(JIPipeGUICommand.class, true);
    }
}
