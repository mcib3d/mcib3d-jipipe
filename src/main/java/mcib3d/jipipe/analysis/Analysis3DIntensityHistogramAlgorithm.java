package mcib3d.jipipe.analysis;


import ij.IJ;
import ij.ImagePlus;
import ij.measure.ResultsTable;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureAbstract;
import mcib3d.geom2.measurements.MeasureIntensity;
import mcib3d.geom2.measurements.MeasureIntensityHist;
import mcib3d.image3d.ImageHandler;
import org.hkijena.jipipe.api.JIPipeCitation;
import org.hkijena.jipipe.api.JIPipeDocumentation;
import org.hkijena.jipipe.api.JIPipeNode;
import org.hkijena.jipipe.api.JIPipeProgressInfo;
import org.hkijena.jipipe.api.annotation.JIPipeTextAnnotation;
import org.hkijena.jipipe.api.data.JIPipeInputDataSlot;
import org.hkijena.jipipe.api.nodes.JIPipeInputSlot;
import org.hkijena.jipipe.api.nodes.JIPipeNodeInfo;
import org.hkijena.jipipe.api.nodes.JIPipeOutputSlot;
import org.hkijena.jipipe.api.nodes.JIPipeParameterSlotAlgorithm;
import org.hkijena.jipipe.api.nodes.categories.ImagesNodeTypeCategory;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.ImagePlusData;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.greyscale.ImagePlusGreyscaleData;
import org.hkijena.jipipe.extensions.tables.datatypes.ResultsTableData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Annotates documentation to the algorithm
@JIPipeDocumentation(name = "3D Analysis Intensity Histogram", description = "Analysis of Intensity Histogram for 2D/3D Data")

// Sets the algorithm category
@JIPipeNode(nodeTypeCategory = ImagesNodeTypeCategory.class, menuPath = "3D Suite")

// Input and output slots autoCreate automatically creates the slots if set to true and no slot configuration was provided
@JIPipeInputSlot(value = ImagePlusGreyscaleData.class, slotName = "Raw", autoCreate = true)
@JIPipeInputSlot(value = ImagePlusGreyscaleData.class, slotName = "Seg", autoCreate = true)
@JIPipeOutputSlot(value = ResultsTableData.class, slotName = "Output", autoCreate = true)

// You can add multiple JIPipeCitation annotations to provide citations for this node only
@JIPipeCitation("3D ImageJ Suite")

public class Analysis3DIntensityHistogramAlgorithm extends JIPipeParameterSlotAlgorithm {

    public Analysis3DIntensityHistogramAlgorithm(JIPipeNodeInfo info) {
        super(info);
    }

    @Override
    public void runParameterSet(JIPipeProgressInfo jiPipeProgressInfo, List<JIPipeTextAnnotation> list) {
        List<JIPipeInputDataSlot> slots =  getNonParameterInputSlots();
        JIPipeInputDataSlot slotRaw = slots.get(0);
        JIPipeInputDataSlot slotSeg = slots.get(1);
        // Run your workload here
        int nbSeg = slotSeg.getAllData(ImagePlusData.class, jiPipeProgressInfo).size();
        int nbRaw = slotRaw.getAllData(ImagePlusData.class, jiPipeProgressInfo).size();
        if(nbSeg != nbRaw) {
            IJ.log("Number of raw inputs must be equal to number of seg inputs");
            return;
        }
        for(int i=0; i<nbSeg; i++){
            ImagePlusData dataSeg = slotSeg.getData(i, ImagePlusData.class, jiPipeProgressInfo);
            ImagePlusData dataRaw = slotRaw.getData(i, ImagePlusData.class, jiPipeProgressInfo);
            runOneImage(dataSeg, dataRaw, jiPipeProgressInfo);
        }
    }

    private void runOneImage(ImagePlusData inputData, ImagePlusData inputDataRaw, JIPipeProgressInfo jiPipeProgressInfo){
        MeasureAbstract measure = new MeasureIntensityHist();
        ImagePlus img = inputData.getDuplicateImage();
        ImagePlus imgRaw = inputDataRaw.getDuplicateImage();
        ImageHandler handler = ImageHandler.wrap(img);
        ImageHandler handlerRaw = ImageHandler.wrap(imgRaw);
        Objects3DIntPopulation population = new Objects3DIntPopulation(handler);
        final String[] measurements = measure.getNamesMeasurement();
        List<Double[]> results = population.getMeasurementsIntensityList(measurements, handlerRaw);
        ResultsTableData tableData = new ResultsTableData();
        for (int r = 0; r < results.size(); r++) {
            int nb = results.get(r).length;
            Map<String, Object> map = new HashMap<>();
            for (int i = 0; i < nb; i++) {
                map.put(measurements[i], results.get(r)[i]);
            }
            tableData.addRow(map);
        }
        //resultsTable.updateResults();
        getFirstOutputSlot().addData(tableData,jiPipeProgressInfo);
    }

    /*
   A deep copy constructor. It is required.
   Please do not forget to deep-copy all important fields
   */
    public Analysis3DIntensityHistogramAlgorithm(Analysis3DIntensityHistogramAlgorithm original) {
        super(original);
        // Deep-copy additional fields here
    }

}
