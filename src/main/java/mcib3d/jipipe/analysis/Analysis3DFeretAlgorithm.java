package mcib3d.jipipe.analysis;


import ij.ImagePlus;
import ij.measure.ResultsTable;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureAbstract;
import mcib3d.geom2.measurements.MeasureEllipsoid;
import mcib3d.geom2.measurements.MeasureFeret;
import mcib3d.image3d.ImageHandler;
import org.hkijena.jipipe.api.JIPipeCitation;
import org.hkijena.jipipe.api.JIPipeDocumentation;
import org.hkijena.jipipe.api.JIPipeNode;
import org.hkijena.jipipe.api.JIPipeProgressInfo;
import org.hkijena.jipipe.api.nodes.*;
import org.hkijena.jipipe.api.nodes.categories.ImagesNodeTypeCategory;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.ImagePlusData;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.d3.greyscale.ImagePlus3DGreyscaleData;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.greyscale.ImagePlusGreyscaleData;
import org.hkijena.jipipe.extensions.tables.datatypes.ResultsTableData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Annotates documentation to the algorithm
@JIPipeDocumentation(name = "3D Analysis Feret", description = "Analysis of Feret for 3D Data")

// Sets the algorithm category
@JIPipeNode(nodeTypeCategory = ImagesNodeTypeCategory.class, menuPath = "3D Suite")

// Input and output slots autoCreate automatically creates the slots if set to true and no slot configuration was provided
@JIPipeInputSlot(value = ImagePlus3DGreyscaleData.class, slotName = "Input", autoCreate = true)
@JIPipeOutputSlot(value = ResultsTableData.class, slotName = "Output", autoCreate = true)

// You can add multiple JIPipeCitation annotations to provide citations for this node only
@JIPipeCitation("3D ImageJ Suite")

public class Analysis3DFeretAlgorithm extends JIPipeSimpleIteratingAlgorithm {

    public Analysis3DFeretAlgorithm(JIPipeNodeInfo info) {
        super(info);
    }

    /*
   A deep copy constructor. It is required.
   Please do not forget to deep-copy all important fields
   */
    public Analysis3DFeretAlgorithm(Analysis3DFeretAlgorithm original) {
        super(original);
        // Deep-copy additional fields here
    }

    @Override
    protected void runIteration(JIPipeDataBatch dataBatch, JIPipeProgressInfo progressInfo) {
        // class
        MeasureAbstract measure = new MeasureFeret();
        // Run your workload here
        ImagePlusData inputData = dataBatch.getInputData(getFirstInputSlot(), ImagePlusData.class, progressInfo);
        ImagePlus img = inputData.getDuplicateImage();
        ImageHandler handler = ImageHandler.wrap(img);
        Objects3DIntPopulation population = new Objects3DIntPopulation(handler);
        final String[] measurements = measure.getNamesMeasurement();
        List<Double[]> results = population.getMeasurementsList(measurements);
        ResultsTableData tableData = new ResultsTableData();
        for (int r = 0; r < results.size(); r++) {
            int nb = results.get(r).length;
            Map<String, Object> map = new HashMap<>();
            for (int i = 0; i < nb; i++) {
                map.put(measurements[i], results.get(r)[i]);
            }
            tableData.addRow(map);
        }
        //resultsTable.updateResults();
        dataBatch.addOutputData(getFirstOutputSlot(), tableData, progressInfo);
    }
}
