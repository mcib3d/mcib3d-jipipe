package mcib3d.jipipe.segmentation;

import net.imagej.ImageJ;
import org.hkijena.jipipe.JIPipe;
import org.hkijena.jipipe.JIPipeDefaultJavaExtension;
import org.hkijena.jipipe.JIPipeGUICommand;
import org.hkijena.jipipe.JIPipeJavaExtension;
import org.hkijena.jipipe.api.JIPipeAuthorMetadata;
import org.hkijena.jipipe.api.JIPipeProgressInfo;
import org.hkijena.jipipe.extensions.parameters.library.markup.HTMLText;
import org.hkijena.jipipe.extensions.parameters.library.primitives.list.StringList;
import org.hkijena.jipipe.utils.ResourceUtils;
import org.hkijena.jipipe.utils.UIUtils;
import org.scijava.Context;
import org.scijava.plugin.Plugin;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Plugin(type = JIPipeJavaExtension.class)
public class Segmentation3DExtension extends JIPipeDefaultJavaExtension {

    @Override
    public String getName() {
        return "3D Segmentation simple algorithm";
    }

    @Override
    public HTMLText getDescription() {
        return new HTMLText("3D Segmentation simple algorithm");
    }

    @Override
    public String getDependencyId() {
        // We recommend the following structure: <groupId>.<artifactId>:<dependencyId>
        // (!) The dependency Id should be unique for your plugin (!)
        return "org.framagit.mcib3d:my-extension";
    }

    @Override
    public String getDependencyVersion() {
        return "1.0.0";
    }

    @Override
    public List<JIPipeAuthorMetadata> getAuthors() {
       JIPipeAuthorMetadata authorMetadata = new JIPipeAuthorMetadata("Dr","Thomas","Boudier",new StringList("AMU"),"https://mcib3d.frama.io/3d-suite-imagej/","thomas boudier",false,true);
        List<JIPipeAuthorMetadata> list = new ArrayList<>();
        list.add(authorMetadata);

        return list;
    }

    @Override
    public String getWebsite() {
        return "https://jipipe.org/";
    }

    @Override
    public String getLicense() {
        return "BSD-2";
    }

    @Override
    public URL getLogo() {
        // This code loads the default JIPipe logo from JIPipe resources
        // You can replace it with your own logo if you want
        // Just do not use JIPipe's ResourceUtils for this, as its always pointing to JIPipe resource directories
        return ResourceUtils.getPluginResource("logo-400.png");
    }

    @Override
    public StringList getDependencyCitations() {
        return new StringList("TANGO");
    }

    @Override
    public String getCitation() {
        // Here you can enter a citation for your publication
        // It will be displayed in the plugin manager
        return "";
    }

    @Override
    public void register(JIPipe jiPipe, Context context, JIPipeProgressInfo progressInfo) {
        // Content is registered here
        registerNodeType("suite3d-segmentation-simple", Segmentation3DSimpleAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-segmentation-hysteresis", Segmentation3DHysteresisAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-segmentation-iterative", Segmentation3DIterativeAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
    }



    public static void main(final String... args) {
        final ImageJ ij = new ImageJ();
        ij.ui().showUI(); // If your ImageJ freezes, you can leave this out. JIPipe will show anyways.
        ij.command().run(JIPipeGUICommand.class, true);
    }

}
