package mcib3d.jipipe.segmentation;

import ij.ImagePlus;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;
import org.hkijena.jipipe.api.JIPipeCitation;
import org.hkijena.jipipe.api.JIPipeDocumentation;
import org.hkijena.jipipe.api.JIPipeNode;
import org.hkijena.jipipe.api.JIPipeProgressInfo;
import org.hkijena.jipipe.api.nodes.*;
import org.hkijena.jipipe.api.nodes.categories.ImagesNodeTypeCategory;
import org.hkijena.jipipe.api.parameters.JIPipeParameter;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.ImagePlusData;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.greyscale.ImagePlusGreyscaleData;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.greyscale.ImagePlusGreyscaleMaskData;

// Annotates documentation to the algorithm
@JIPipeDocumentation(name = "3D Segmentation Simple", description = "3D Simple Labelling")

// Sets the algorithm category
@JIPipeNode(nodeTypeCategory = ImagesNodeTypeCategory.class, menuPath = "3D Suite")

// Input and output slots autoCreate automatically creates the slots if set to true and no slot configuration was provided
@JIPipeInputSlot(value = ImagePlusGreyscaleMaskData.class, slotName = "Input", autoCreate = true)
@JIPipeOutputSlot(value = ImagePlusGreyscaleData.class, slotName = "Output", autoCreate = true)

// You can add multiple JIPipeCitation annotations to provide citations for this node only
@JIPipeCitation("Additional citation")

public class Segmentation3DSimpleAlgorithm extends JIPipeSimpleIteratingAlgorithm {
    double minSize = 10;
    double maxSize = -1;

    /*
        This is the main constructor of the algorithm.
        It contains a reference to the algorithm info that contains
        some important metadata
        */
    public Segmentation3DSimpleAlgorithm(JIPipeNodeInfo info) {
        super(info);
    }

    @Override
    protected void runIteration(JIPipeDataBatch dataBatch, JIPipeProgressInfo progressInfo) {
        // Get Image Handler
        ImagePlusData inputData = dataBatch.getInputData(getFirstInputSlot(), ImagePlusData.class, progressInfo);
        ImagePlus img = inputData.getDuplicateImage();
        ImageHandler handler = ImageHandler.wrap(img);
        // do things
        if(maxSize == -1) maxSize = Double.POSITIVE_INFINITY;
        ImageLabeller labeller = new ImageLabeller(minSize, maxSize);
        ImagePlus img2 = labeller.getLabels(handler).getImagePlus();
        // get result
        dataBatch.addOutputData(getFirstOutputSlot(),new ImagePlusData(img2),progressInfo);
    }

    /*
    A deep copy constructor. It is required.
    Please do not forget to deep-copy all important fields
    */
    public Segmentation3DSimpleAlgorithm(Segmentation3DSimpleAlgorithm original) {
        super(original);
        // Deep-copy additional fields here
        this.minSize = original.minSize;
        this.maxSize = original.maxSize;
    }

    public boolean supportsParallelization() {
        return false;
    }


    @JIPipeDocumentation(name = "Minimum Size", description = "Minimum size of detected objects (in pixels).")
    @JIPipeParameter("minSize")
    public double getMinSize() {
        return minSize;
    }

    @JIPipeParameter("minSize")
    public void setMinSize(double size) {
        this.minSize = size;
    }

    @JIPipeDocumentation(name = "Maximum Size", description = "Maximum size of detected objects (in pixels), -1 for no limit.")
    @JIPipeParameter("maxSize")
    public double getMaxSize() {
        return maxSize;
    }

    @JIPipeParameter("maxSize")
    public void setMaxSize(double size) {
        this.maxSize = size;
    }
}

